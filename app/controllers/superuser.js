/**
 * Created by Menil on 7/25/2016.
 * Admins controls
 */
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Category = mongoose.model('Category');
var Report = mongoose.model('Report');
var sharp = require('sharp');

// GET /login
exports.loginAdmin = function (req, res) {
    res.status(200);
    res.json({
        message: "Admin successfully logged in"
    })
};

// GET /admin/profile
exports.getUserProfiles = function (req, res) {
    User.find({"visible": true}, {"__v": false}, function (err, users) {
        if (err) {
            returnError(500, "Something went wrong. Please try again", res);
        }
        else {
            res.status(200);
            res.json({
                users: users
            });
        }
    });
};

// DELETE /admin/profile/:id
exports.deleteUserProfile = function (req, res) {
    User.remove({"_id": req.params.id}, function (err) {
        if (err) {
            returnError(500, "Something went wrong. Please try again", res);
        } else {
            res.status(200);
            res.json({
                message: "User successfully deleted"
            })
        }
    })
};

// PUT /admin/profile/ban/:id
exports.banUserProfile = function (req, res) {
    User.findById(req.params.id, function (err, user) {
        if (!user) {
            returnError(403, "User does not exist", res);
        } else {
            user.banStatus = !user.banStatus;
            user.save(function (err) {
                if (err) {
                    returnError(500, "Something went wrong. Please try again", res);
                } else {
                    res.status(200);
                    if (user.banStatus) {
                        res.json({
                            message: "User banned!"
                        })
                    } else {
                        res.json({
                            message: "User taken from ban list!"
                        })
                    }
                }
            })
        }
    })
};

// POST /admin/category/:id
exports.editCategory = function (req, res) {
    console.log(req.params.id);
    Category.findByIdAndUpdate(req.params.id, req.body, {new: true}, function (err, category) {
        if (err) {
            returnError(500, "Something went wrong. Please try again", res);
        } else {
            res.status(200);
            res.json({
                message: "Successfully updated category"
            })
        }
    });
};

// GET /admin/category

exports.getCategories = function (req, res) {
    Category.find({"visible": true}, {"__v": false}, function (err, categories) {
        if (err) {
            res.status(500);
            res.json({
                message: "Error getting categories"
            });
        }
        else {
            res.status(200);
            res.json({
                categories: categories
            });
        }
    });
};

// POST /admin/category
exports.createCategory = function (req, res) {
    var CategoryModel = new Category(req.body);
    Category.findOne({'name': req.body.name}, {"__v": false, "_id": false}, function (err, category) {
        console.log("findOne");
        if (err) {
            returnError(500, "Something went wrong. Please try again", res);
        } else if (category !== null) {
            // found existing category
            returnError(403, "Category already exists", res);
        } else {
            CategoryModel.save(function (err) {
                if (err) {
                    returnError(500, "Something went wrong. Please try again", res);
                } else {
                    res.status(201);
                    res.json({
                        message: "Category successfully created"
                    });
                }
            })
        }
    })
};
// DELETE /admin/category/:id

exports.deleteCategory = function (req, res) {
    Category.remove({"_id": req.params.id}, function (err) {
        if (err) {
            returnError(500, "Something went wrong. Please try again", res);
        } else {
            res.status(200);
            res.json({
                message: "Category successfully deleted"
            })
        }
    })
};

// GET /admin/report/:page?sort=''&crit=''
// http://localhost:3000/admin/report/1?sort=title&crit=1  by title ascending
// http://localhost:3000/admin/report/1?sort=title&crit=-1  by title descending
exports.getReports = function (req, res) {
    var pageNumber;
    var limitNumber;
    var query = require('url').parse(req.url, true).query;
    var sorting;
    if (query.sort == 'date') {
        if (query.crit == 1) {
            sorting = 'date'
        } else {
            sorting = '-date'
        }
    } else if (query.sort == 'title') {
        sorting = {title: query.crit}
    } else if (query.sort == 'approved') {
        sorting = {aprovedStatus: query.crit}
    } else if (query.sort == 'resolved') {
        sorting = {resolveStatus: query.crit}
    }

    limitNumber = 20;
    pageNumber = (Number(req.params.page) - 1) * limitNumber;


    // get reports sorted by criteria
    Report.find(null, function (err, reports) {
        if (err) {
            returnError(500, "Something went wrong try again", res);
        } else {
            res.status(200);
            res.json({
                reports: reports
            })
        }
    }).sort(sorting).limit(limitNumber).skip(pageNumber);
};

// PUT /admin/report/approve/:id
exports.approveReport = function (req, res) {
    Report.findById(req.params.id, function (err, report) {
        if (!report) {
            returnError(403, "Report does not exist", res);
        } else {

            var newPath = "./uploads/fullsize/" + req.params.id + ".png";
            var thumbPath = "./uploads/thumbs/" + req.params.id + ".png";

            if (report.aprovedStatus) {
                report.aprovedStatus = false;
                sharp(newPath)
                    .resize(300, 300)
                    .toFormat('png')
                    // .background({r: 192, g: 192, b: 192, a: 1})
                    .background('#C0C0C0')
                    .overlayWith('./uploads/u_obradi.png')
                    // .max()
                    // .blur(0.5)
                    // .embed()
                    .toFile(thumbPath, function (err) {
                        if (err) {
                            console.log(err);
                            returnError(500, "Could not resize image", res);
                        } else {
                            console.log("resize success");
                            report.save(function (err) {
                                if (err) {
                                    returnError(500, "Something went wrong. Please try again", res);
                                } else {
                                    res.status(200);
                                    res.json({
                                        message: "Successfully unapproved report"
                                    })
                                }
                            });
                        }
                    });
            } else {
                report.aprovedStatus = true;
                sharp(newPath)
                    .resize(300, 300)
                    .toFormat('png')
                    // .background({r: 192, g: 192, b: 192, a: 1})
                    .background('#C0C0C0')
                    //.overlayWith('./uploads/z_popravljeno.png')
                    // .max()
                    // .blur(0.5)
                    // .embed()
                    .toFile(thumbPath, function (err) {
                        if (err) {
                            console.log(err);
                            returnError(500, "Could not resize image", res);
                        } else {
                            console.log("resize success");
                            report.save(function (err) {
                                if (err) {
                                    returnError(500, "Something went wrong. Please try again", res);
                                } else {
                                    res.status(200);
                                    res.json({
                                        message: "Successfully approved report"
                                    })
                                }
                            });
                        }
                    });
            }
        }
    })
};

// PUT /admin/report/resolve/:id
exports.resolveReport = function (req, res) {
    Report.findById(req.params.id, function (err, report) {
        if (err) {
            returnError(500, "Something went wrong. Please try again", res);
        } else {
            var newPath = "./uploads/fullsize/" + req.params.id + ".png";
            var thumbPath = "./uploads/thumbs/" + req.params.id + ".png";

            if (report.resolveStatus) {
                report.resolveStatus = false;
                sharp(newPath)
                    .resize(300, 300)
                    .toFormat('png')
                    // .background({r: 192, g: 192, b: 192, a: 1})
                    .background('#C0C0C0')
                    // .overlayWith('./uploads/z_popravljeno.png')
                    // .max()
                    // .blur(0.5)
                    // .embed()
                    .toFile(thumbPath, function (err) {
                        if (err) {
                            console.log(err);
                            returnError(500, "Could not resolve, try again.", res);
                        } else {
                            console.log("resize success");

                            report.save(function (err) {
                                if (!err) {
                                    updateUserProfileFixCounter(report.reportedBy, false, res)
                                } else {
                                    returnError(500, "Could not resolve, try again.", res);
                                }
                            });

                        }
                    });
            } else {
                report.resolveStatus = true;
                sharp(newPath)
                    .resize(300, 300)
                    .toFormat('png')
                    // .background({r: 192, g: 192, b: 192, a: 1})
                    .background('#C0C0C0')
                    .overlayWith('./uploads/z_popravljeno.png')
                    // .max()
                    // .blur(0.5)
                    // .embed()
                    .toFile(thumbPath, function (err) {
                        if (err) {
                            console.log(err);
                            returnError(500, "Could not resolve, try again.", res);
                        } else {
                            report.save(function (err) {
                                if (!err) {
                                    updateUserProfileFixCounter(report.reportedBy, true, res)
                                } else {
                                    returnError(500, "Could not resolve, try again.", res);
                                }
                            });
                        }
                    });
            }
        }
    });
};

function updateUserProfileFixCounter(userId, fixed, res) {
    User.findById(userId, function (err, user) {
        if (!err) {

            var messageString;
            if (user !== null) {
                if (fixed) {
                    user.fixCount++;
                } else {
                    user.fixCount--;
                }
                user.save(function (err) {
                    if (!err) {
                        res.status(200);
                        if (fixed) {
                            messageString = "Successfully resolved report"
                        } else {
                            messageString = "Successfully unresolved report"
                        }
                        res.json({
                            message: messageString
                        })
                    } else {
                        returnError(500, "Something went wrong try again", res);
                    }
                });
            } else {
                if (fixed) {
                    messageString = "Successfully resolved report"
                } else {
                    messageString = "Successfully unresolved report"
                }
                res.json({
                    message: messageString
                })
            }
        } else {
            returnError(500, "Something went wrong try again", res);
        }
    });

}

// DELETE /admin/report/:id
exports.deleteReport = function (req, res) {
    Report.remove({"_id": req.params.id}, function (err) {
        if (err) {
            returnError(500, "Something went wrong. Please try again", res);
        } else {
            res.status(200);
            res.json({
                message: "Report successfully deleted"
            })
        }
    })
};

function returnError(statusCode, message, res) {
    res.status(statusCode);
    res.json({
        message: message
    })
}