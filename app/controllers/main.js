/**
 * Created by Menil on 7/25/2016.
 * General controls
 */
exports.pingService = function (req, res) {
    console.log("Pinging...");
    res.status(200);
    res.json({
        "message": "Service is up and running"
    });
};

exports.notFound = function (req, res) {
    res.status(404);
    res.json({
        "message": 'not found',
        url: req.originalUrl
    })
};

exports.checkVersionAndContentType = function (req, res, next) {
    var version = "";
    if (req.headers["accept"]) {
        version = req.headers["accept"].split("-")[1].split("+")[0];
    }
    req.version = version;
    next()
};