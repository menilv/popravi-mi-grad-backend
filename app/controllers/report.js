/**
 * Created by Menil on 7/25/2016.
 * Report controls
 */
var mongoose = require('mongoose');
var User = mongoose.model("User");
var Report = mongoose.model("Report");
var Category = mongoose.model("Category");

// GET /report/:type/:page
// GET /report/:id
exports.getReports = function (req, res, next) {
    var searchParams;
    var query = require('url').parse(req.url, true).query;
    var pageNumber;
    var limitNumber;

    // if is 'map' request return last 100 reports
    if (req.params.page === 'map') {
        pageNumber = 0;
        limitNumber = 100;
    } else {
        limitNumber = 20;
        pageNumber = (Number(req.params.page) - 1) * limitNumber;

    }

    // get all reports
    // or get user reports
    // or get user report by ID
    if (req.params.type == "all") {
        searchParams = {aprovedStatus: true};
    } else if (req.params.type == "mine") {
        searchParams = {reportedBy: req.user._id};
        if (query.approved) {
            searchParams.aprovedStatus = query.approved;
        }
    } else {
        searchParams = {_id: req.params.id};
        limitNumber = 1;
        pageNumber = 0;
        updateView(req, res, next)
    }


    // get reports sorted by date
    Report.find(searchParams, function (err, reports) {
        if (err) {
            returnError(500, "Something went wrong try again", res);
        } else {
            res.status(200);
            res.json({
                reports: reports
            })
        }
    }).sort('-date').limit(limitNumber).skip(pageNumber);
};

// GET report/category/:type/:page
exports.getReportsByCategory = function (req, res) {
    var searchParams;
    var pageNumber;
    var limitNumber;

    if (req.params.page === 'map') {
        pageNumber = 0;
        limitNumber = 100;
    } else {
        limitNumber = 20;
        pageNumber = (Number(req.params.page) - 1) * limitNumber;
    }

    // get all approved reports by category ID
    searchParams = {
        $and: [{'category._id': new ObjectId(req.params.type)},
            {aprovedStatus: true}]
    };
    //
    // get reports sorted by date
    Report.find(searchParams, function (err, reports) {
        if (err) {
            returnError(500, "Something went wrong try again", res);
        } else {
            res.status(200);
            res.json({
                reports: reports
            })
        }
    }).sort('-date').limit(limitNumber).skip(pageNumber);
};

// POST /report
exports.createReport = function (req, res) {
    // check for user and category ids
    var categoryId = req.body.category._id;
    if (!categoryId) {
        returnError(400, "Category is missing", res);
    } else {
        Category.findById(categoryId, function (err, category) {
            if (!category) {
                returnError(400, "Category is missing", res);
            } else {
                req.body.reportedBy = req.user._id;
                var report = new Report(req.body);
                report.category = category;
                report.save(function (err, report) {
                    if (err) {
                        returnError(500, "Something went wrong try again", res);
                    } else {
                        updateUserProfileCounter(req, report, res);
                    }
                });
            }
        }).select({"__v": false, "visible": false});
    }
};

// POST /report/like/:id
exports.updateLike = function (req, res) {
    checkReportId(req);

    Report.findById(req.params.id, function (err, report) {
        if (err) {
            returnError(500, "Something went wrong try again", res);
        } else {

            User.findById(req.user._id, function (err, user) {
                if (err) {
                    returnError(500, "User not found", res);
                } else {
                    var addExtra = true;
                    for (var i = 0; i < user.reportsLiked.length; i += 1) {
                        if (report._id == user.reportsLiked[i]) {
                            addExtra = false;
                            user.reportsLiked.splice(i, 1);
                            saveWithNewCount(report, -1, res);
                            break;
                        }
                    }
                    if (addExtra) {// did not found it within liked posts; add to liked
                        user.reportsLiked.push(report._id);
                        saveWithNewCount(report, 1, res);
                    }

                    user.save(function (err) {
                        if (err) {
                            returnError(500, "Something went wrong try again3", res);
                        }
                    });
                }
            });
        }
    })
};

exports.updateViewCount = function (req, res, next) {
    updateView(req, res, next);
};

function updateView(req, res, next) {
    var id = req.params.type;
    if (id == null)
        id = req.params.id;
    Report.findById(id, function (err, report) {
        if (err) {
            returnError(500, "Something went wrong try again", res);
        } else {
            report.viewCount = report.viewCount + 1;
            report.save(function (err) {
                if (err) {
                    returnError(500, "Something went wrong try again", res);
                } else if (req.params.id != null) {
                    res.status(200);
                    res.json({
                        message: "Updated view count"
                    });
                } else
                    next()
            });
        }
    })
}

function checkReportId(req) {
    var reportId = req.params.id;
    if (!reportId) {
        res.status(400);
        res.json({
            message: "Missing report id"
        });
    }
}

function updateUserProfileCounter(req, report, res) {
    var user = req.user;
    user.reports.push(report._id);
    user.reportCount++;
    user.save(function (err) {
        if (!err) {
            res.status(201);
            res.json({
                message: report._id
            })
        } else {
            returnError(500, "Something went wrong try again", res);
        }
    });
}

function saveWithNewCount(report, countChange, res) {
    report.likeCount = report.likeCount + countChange;
    report.save(function (err) {
        if (!err) {
            res.status(200);
            res.json({
                message: "Like count changed"
            })
        } else {
            returnError(500, "Something went wrong try again", res);
        }
    });
}

function returnError(statusCode, message, res) {
    res.status(statusCode);
    res.json({
        message: message
    })
}