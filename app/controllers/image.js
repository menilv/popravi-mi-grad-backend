/**
 * Created by Menil on 7/25/2016.
 * Image controls
 */
var fs = require('fs');
var sharp = require('sharp');
var mongoose = require('mongoose');
var Report = mongoose.model("Report");

// POST /upload/:reportId
exports.uploadFile = function (req, res) {
    fs.readFile(req.files.image.path, function (err, data) {
        var imageName = req.files.image.name;
        console.log(imageName);
        if (!imageName) {
            res.status(400);
            res.json({
                message: "No image found"
            })
        } else {
            var newPath = "./uploads/fullsize/" + req.params.reportId + ".png";
            var thumbPath = "./uploads/thumbs/" + req.params.reportId + ".png";
            fs.writeFile(newPath, data, function (err) {
                // write file to uploads/thumbs folder
                if (err) {
                    // console.log(err);
                    res.status(500);
                    res.json({
                        message: "Cannot write image"
                    })
                } else {
                    sharp(newPath)
                        .resize(300, 300)
                        .toFormat('png')
                        // .background({r: 192, g: 192, b: 192, a: 1})
                        .background('#C0C0C0')
                        .overlayWith('./uploads/u_obradi.png')
                        // .max()
                        // .blur(0.5)
                        // .embed()
                        .toFile(thumbPath, function (err) {
                            if (err) {
                                console.log(err);
                            } else {
                                console.log("resize success");
                            }
                        });

                    Report.findByIdAndUpdate(req.params.reportId, {$set: {image: newPath}}, function (err, report) {
                        if (!err) {
                            res.status(201);
                            res.json({
                                //report: report,
                                message: "Successfully uploaded image"
                            })
                        } else {
                            res.status(500);
                            res.json({
                                message: "Cannot resize image, try again"
                            });
                        }
                    });
                }
            });
        }
    })
};

// PUT /upload/category/:name
exports.uploadIcon = function (req, res) {
    fs.readFile(req.files.image.path, function (err, data) {
        var imageName = req.params.name;
        console.log(imageName);
        if (!imageName) {
            res.status(400);
            res.json({
                message: "No icon found"
            })
        } else {
            var newPath = "./images/category/" + imageName + ".png";
            fs.writeFile(newPath, data, function (err) {
                // write file to uploads/thumbs folder
                if (err) {
                    res.status(400);
                    res.json({
                        message: "Cannot write icon"
                    });
                } else {
                    res.status(201);
                    res.json({
                        message: "Successfully uploaded icon"
                    })
                }
            });
        }
    });
};

// GET /upload/:size/:reportId
exports.downloadFile = function (req, res) {
    var size = req.params.size;
    var img = fs.readFileSync("./uploads/" + size + "/" + req.params.reportId + ".png");

    res.writeHead(200, {'Content-Type': 'image/png'});
    res.end(img, 'binary');
};

// GET /upload/category/:name
exports.downloadIcon = function (req, res) {
    var name = req.params.name;
    var img = fs.readFileSync("./images/category/" + name + ".png");

    res.writeHead(200, {'Content-Type': 'image/png'});
    res.end(img, 'binary');
};