/**
 * Created by Menil on 7/25/2016.
 * User controls
 */
var mongoose = require('mongoose');
var User = mongoose.model("User");
var auth = require('basic-auth');

var request = require('request');


exports.checkUser = function (req, res, next, authUser) {
    User.find({$and: [{"email": authUser.name}, {"token": authUser.pass}]}, {"__v": false}, function (err, user) {
        if (err) {
            res.status(500);
            res.json({
                "message": "Error occurred " + err
            })
        } else if (user[0] && user[0].email === authUser.name && user[0].token === authUser.pass && !user[0].banStatus) {
            // success move to endpoint
            req.user = user[0];
            next(user[0])
        } else {
            console.log(user[0]);
            res.status(401);
            if (user[0] != null && user[0].banStatus) {
                res.json({
                    "message": "User banned!"
                })
            } else
                res.json({
                    "message": "Authorization failed"
                })
        }
    }).limit(1);
};

// POST /user/register
exports.registerPhone = function (req, res) {
    User.find({email: req.body.email}, function (err, oldUserModel) {
        // phone number already registered generate code and post
        var code = random(10000, 100000);
        request({
            url: 'https://www.experttexting.com/exptapi/exptsms.asmx/SendSMS',
            method: 'POST',
            form: {
                UserID: 'bkapetanovic',
                PWD: 'sifra23',
                APIKEY: 'dw7bov329t79ptl',
                FROM: 'DEFAULT',
                TO: req.body.email,
                MSG: code
            }
        }, function (error, response, body) {
            if (error) {
                console.log(error);
            } else {
                console.log(response.statusCode, body);
            }
        });
        // request.post({
        //     'https://www.experttexting.com/exptapi/exptsms.asmx/SendSMS',
        //     // .form({UserID: 'bkapetanovic'})
        //     // .form({PWD: 'sifra23'})
        //     // .form({APIKEY: 'dw7bov329t79ptl'})
        //     // .form({FROM: 'DEFAULT'})
        //     // .form({TO: req.body.email})
        //     // .form({MSG: code});
        // { form: {UserID: 'bkapetanovic' }, { PWD: 'sifra23' }, { APIKEY: 'dw7bov329t79ptl' }, { FROM: 'SMS' }, { TO: req.body.email}, { MSG: code} }},
        // function (error, response, body) {
        // if (!error && response.statusCode == 200) {
        //     console.log(body);
        // }
        // }
        // );

        if (err) {
            res.status(500);
            res.json({
                "message": "Error occurred: " + err
            })
        } else if (oldUserModel[0]) {
            oldUserModel[0].token = code;
            oldUserModel[0].save(function (err) {
                if (err) {
                    res.status(500);
                    res.json({
                        "message": "Error occurred: " + err
                    })
                } else {
                    res.status(200);
                    res.json({
                        "message": "Successfully logged user"
                    })
                }
            });
        } else {
            // save user
            var UserModel = new User(req.body);
            UserModel.updatedAt = new Date();
            UserModel.token = code;
            UserModel.save(function (err) {
                if (err) {
                    res.status(500);
                    res.json({
                        "message": "Error occurred: " + err
                    })
                } else {
                    res.status(200);
                    res.json({
                        "message": "Successfully registered user"
                    })
                }
            })
        }
    })
        .limit(1);
};


function random(low, high) {
    return Math.round((Math.random() * (high - low) + low));
}

// POST /user
exports.loginUser = function (req, res) {
    var UserModel = new User(req.body);
    User.find({email: req.body.email}, function (err, oldUserModel) {

        if (err) {
            res.status(500);
            res.json({
                "message": "Error occurred: " + err
            })
        } else if (oldUserModel[0]) {
            //always use model[0] when using find().limit(1) to get the element from db
            // update User token
            oldUserModel[0].token = req.body.token;
            // save updated User
            oldUserModel[0].save(function (err) {
                if (err) {
                    res.status(500);
                    res.json({
                        "message": "Error occurred: " + err
                    })
                } else {
                    res.status(200);
                    res.json({
                        "message": "Successfully logged user"
                    })
                }
            })
        } else {
            // save user
            UserModel.updatedAt = new Date();
            UserModel.save(function (err) {
                if (err) {
                    res.status(500);
                    res.json({
                        "message": "Error occurred: " + err
                    })
                } else {
                    res.status(200);
                    res.json({
                        "message": "Successfully registered user"
                    })
                }
            })
        }
    })
        .limit(1)
};

// GET /profile
exports.getUserProfile = function (req, res) {
    res.status(200);
    res.json({
        user: req.user
    })
};

// PUT /profile
exports.editUserProfile = function (req, res) {
    // update time
    req.body.updatedAt = new Date();
    User.findByIdAndUpdate(req.user._id, req.body, {new: true}, function (err, user) {
        if (err) {
            res.status(500);
            res.json({
                message: "Cannot update user"
            });
        } else {
            // user deleted
            if (user.visible == true) {
                res.status(203);
                res.json(user)
            } else {
                // user edited
                res.status(200);
                res.json({
                    message: "User deleted"
                });
            }
        }
    })
};
