/**
 * Created by Menil on 7/25/2016.
 * Category controls
 */
var mongoose = require('mongoose');
var Category = mongoose.model("Category");
ObjectId = mongoose.Types.ObjectId;

// GET /category
exports.getCategories = function (req, res) {
    Category.find({"visible": true}, {"__v": false}, function (err, categories) {
        if (err) {
            res.status(500);
            res.json({
                message: "Error getting categories"
            });
        }
        else {
            res.status(200);
            res.json({
                categories: categories
            });
        }
    });
};