var mongoose = require("mongoose");
var Schema   = mongoose.Schema;

var CategorySchema = new Schema({
    name: String,
    icon: String,
    visible: {type: Boolean, default: true}
});

CategorySchema.set('toJSON', {
    transform: function(doc, ret, options) {
        delete ret.visible;
        delete ret.__v;

        return ret;
    }
});

mongoose.model('Category', CategorySchema);

