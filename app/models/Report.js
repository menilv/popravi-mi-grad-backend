var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var User = require('./../models/User.js');
var Category = require('./../models/Category.js');

var ReportSchema = new Schema({
    title: String,
    image: String,
    description: {type: String, default: "Nema opisa"},
    longitude: Number,
    latitude: Number,
    address: String,
    resolveStatus: {type: Boolean, default: false},
    aprovedStatus: {type: Boolean, default: false},
    date: {type: Date, default: Date.now},
    likeCount: {type: Number, min: 0, default: 0},
    viewCount: {type: Number, min: 0, default: 0},
    visible: {type: Boolean, default: true},
    category: Category,
    reportedBy: {type: Schema.ObjectId, ref: "User"}
});

ReportSchema.set('toJSON', {
    transform: function(doc, ret, options) {
        delete ret.visible;
        delete ret.__v;

        return ret;
    }
});

mongoose.model('Report', ReportSchema);