var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var UserSchema = new Schema({
    firstname: String,
    lastname: String,
    image: String,
    email: String,
    reportCount: {type: Number, default: 0},
    fixCount: {type: Number, default: 0},
    banStatus: {type: Boolean, default: false},
    token: String,
    createdAt: {type: Date, default: Date.now},
    updatedAt: Date,
    visible: {type: Boolean, default: true},
    reports: [{type: Schema.ObjectId, ref: "Report"}],
    reportsLiked: [String]
});

UserSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        delete ret.visible;
        delete ret.__v;
        // delete ret.reportsLiked;

        return ret;
    }
});

mongoose.model('User', UserSchema);