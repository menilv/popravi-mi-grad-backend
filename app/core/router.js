/**
 * Created by Menil on 7/25/2016.
 */
var restify = require('restify');
var fs = require('fs');
var auth = require('basic-auth');
var mongoose = require('mongoose');
const corsMiddleware = require('restify-cors-middleware');

var controllers = {}
    , controllers_path = process.cwd() + '/app/controllers';
fs.readdirSync(controllers_path).forEach(function (file) {
    if (file.indexOf('.js') != -1) {
        controllers[file.split('.')[0]] = require(controllers_path + '/' + file)
    }
});
var server = restify.createServer();
const cors = corsMiddleware({
    preflightMaxAge: 5,
    allowHeaders: ['Access-Control-Allow-Origin', 'Authorization']
});

server.pre(cors.preflight);

server
    .use(restify.fullResponse())
    .use(restify.bodyParser())
    .use(function (req, res, next) {
        console.log(req);
        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', '*');

        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');

        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', true);

        if (process.env.environment === 'debug') {
            console.log(req.method);
            console.log(req.url);
        }
        if (req.url.match(/^\/admin\/.+/) !== null || req.url.match(/^\/web\/.+/) !== null ||
            (req.url.match(/^\/upload\/.+/) !== null) ||
            (req.method === 'POST' && req.url === '/register' ) ||
            (req.method === 'POST' && req.url === '/user') ||
            (req.method === 'GET' && req.url === '/ping')) {
            // registration doesn't need basic auth
            next()
        } else {
            var authUser = auth(req);
            if (authUser) {
                if (authUser.name == "admin" && authUser.pass == "admin") {
                    // admin user hardcoded for now
                    // should check admin in database
                    next();
                }
                controllers.user.checkUser(req, res, next, authUser);
            } else {
                // next();
                res.status(403);
                res.json({
                    "message": "Authorization missing"
                })
            }
        }
    });

// Static endpoints
server.get(/\/web\/?.*/, restify.serveStatic({
    'directory': __dirname + '/../../web',
    'default': 'Login.html'
}));

// server.get(/.*/, restify.serveStatic({
//     'directory': __dirname+'/../../web',
//     'default': 'Login.html'
// }));
// General endpoints
server.get("/ping", controllers.main.pingService);

// Resource endpoints
server.post("/upload/:reportId", controllers.image.uploadFile); // upload image for report
server.get("/upload/:size/:reportId", controllers.image.downloadFile); // download image
server.get("/upload/:name", controllers.image.downloadIcon); // download category icon
server.put("/upload/category/:name", controllers.image.uploadIcon); // upload icon for category

// User endpoints
server.post("/user", controllers.user.loginUser); // register user via facebook/google
server.post("/register", controllers.user.registerPhone); // register via phone number
server.get("/profile", controllers.user.getUserProfile); // get user profile
server.put("/profile", controllers.user.editUserProfile); // edit user profile

// Admin endpoints
server.get("/login", controllers.superuser.loginAdmin); // Admin login
server.get("/admin/profile", controllers.superuser.getUserProfiles); // get user profiles
server.del("/admin/profile/:id", controllers.superuser.deleteUserProfile); // Delete user profile
server.put("/admin/profile/ban/:id", controllers.superuser.banUserProfile); // Ban user profile
server.get("/admin/category", controllers.superuser.getCategories); // Get categories
server.post("/admin/category/:id", controllers.superuser.editCategory); // edit single category
server.post("/admin/category", controllers.superuser.createCategory); // creates a new category
server.del("/admin/category/:id", controllers.superuser.deleteCategory); // deletes category
server.get("admin/report/:page", controllers.superuser.getReports); // Get all reports
server.put("/admin/report/approve/:id", controllers.superuser.approveReport); // Approve report
server.put("/admin/report/resolve/:id", controllers.superuser.resolveReport); // Resolve report
server.del("/admin/report/:id", controllers.superuser.deleteReport); // Delete report

// Category endpoints
server.get("/category", controllers.category.getCategories); // retrieve all categories

// Report endpoints
server.get("/report/:type/:page", controllers.report.getReports); // get user/all reports
server.get("/report/:id", controllers.report.getReports); // get report by ID
server.get("/report/category/:type/:page", controllers.report.getReportsByCategory)
server.post("/report", controllers.report.createReport); // create report for user
server.post("/report/like/:id", controllers.report.updateLike); // changes like count
server.post("/report/view/:id", controllers.report.updateViewCount); // update report view count

var port;
var adr = process.env.OPENSHIFT_NODEJS_IP;
if (typeof adr === "undefined") {
    //  Log errors on OpenShift but continue w/ 127.0.0.1 - this
    //  allows us to run/test the app locally.
    console.warn('No OPENSHIFT_NODEJS_IP var, using localhost');
    // adr = 'localhost';
    adr = '0.0.0.0';
}

ipaddress = adr;
if (process.env.environment === 'debug') {
    port = process.env.PORT || 3000;
} else {
    port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
}
server.listen(port, ipaddress, function (err) {
    if (err)
        console.error(err);
    else {
        console.log('App is ready at : ' + port)
        console.log('DB is ready at: ' + 27017)
    }
});

if (process.env.environment == 'production')
    process.on('uncaughtException', function (err) {
        console.error(JSON.parse(JSON.stringify(err, ['stack', 'message', 'inner'], 2)))
    });
