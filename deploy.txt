#mkdir popravimiopcinu #only if first time
#sudo git clone https://menilv@bitbucket.org/menilv/popravi-mi-grad-backend.git #only if first time
cd popravimiopcinu
#sudo apt-get install -y git #only if not installed
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
echo '==============================update packages=============================='
sudo apt-get update
#echo '==============================install mongo=============================='
#sudo apt-get install -y mongodb-org #only if first time
#mkdir data #only if first time
cd data
#mkdir db #only if first time
cd ..
echo '==============================start mongo deamon=============================='
mongod --fork --logpath data/db/mongodb.log --port 2999 --dbpath data/db
echo '==============================installing nodejs=============================='
#sudo apt-get install -y nodejs #only if first time
echo '==============================intalling npm=============================='
#sudo apt-get install -y node #only if first time
sudo npm cache clean -f
sudo npm install -g n
sudo n stable
#sudo n latest #optional to upgrade to latest instead of stable
sudo ln -sf /usr/local/n/versions/node/6.2.2/bin/node /usr/bin/node 
echo '==============================intalling npm=============================='
#sudo apt-get install -y npm #only if first time
cd popravi-mi-grad-backend
sudo npm i
echo '==============================starting app=============================='
#start script:
forever start -c nodemon server.js