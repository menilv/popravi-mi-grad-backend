/**
 * Created by Menil on 7/25/2016.
 */
process.env['environment'] = 'debug';
process.env['MONGO_URL'] = 'mongodb://127.0.0.1:27017/popravimiopcinu';

// process.env['MONGO_URL'] = process.env.OPENSHIFT_MONGODB_DB_URL+'popravimigrad';
// process.env['environment'] = 'production';

require('./app/core/mongoose');
require('./app/core/router');
